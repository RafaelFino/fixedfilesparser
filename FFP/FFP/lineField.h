#pragma once
#include <string>
#include <iostream>
#include <iomanip>

class lineField
{
public:
	int Start;
	int Len;
	std::string Name;
	std::string Target;
	std::string Format;
	std::string Rawdata;

	lineField();
	~lineField();

	std::string GetFormated();
	bool SetValue(const std::string& rawdata);
};

