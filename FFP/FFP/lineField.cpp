#include "lineField.h"

using namespace std;

lineField::lineField()
{
}


lineField::~lineField()
{
}

std::string lineField::GetFormated()
{
	return Rawdata;
}

bool lineField::SetValue(const std::string& data)
{
	bool ret = false;

	if (data.length() > (size_t)(Start + Len))
	{
		Rawdata = data.substr(Start, Len);
		ret = true;
	}
	
	return ret;
}
