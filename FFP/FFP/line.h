#pragma once
#include <iostream>
#include <vector>

#include "lineField.h"

class line
{
private:
	std::string rawdata_;

public:
	line();
	~line();

	std::vector<lineField> Fields;
	std::string GetRawdata();
};

